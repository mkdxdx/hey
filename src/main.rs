#![no_std]
#![no_main]

// pick a panicking behavior
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics
// use panic_abort as _; // requires nightly
// use panic_itm as _; // logs messages over ITM; requires ITM support
// use panic_semihosting as _; // logs messages to the host stderr; requires a debugger

use cortex_m::asm;
use cortex_m_rt::entry;
use cortex_m;
use stm32f3xx_hal as hal;
use hal::pac;
use hal::pwm::tim1;
use hal::prelude::*;
use hal::spi::Spi;

#[entry]
fn main() -> ! {

    let periph = pac::Peripherals::take().unwrap();

    let mut rcc = periph.RCC.constrain();
    let mut flash = periph.FLASH.constrain();

    let clocks = rcc.cfgr
        .use_hse(12.MHz())
        .hclk(72.MHz())
        .sysclk(72.MHz())
        .pclk1(36.MHz())
        .pclk2(72.MHz())
        .freeze(&mut flash.acr);

    // set up blinking 100ms pwm on PE9 via TIM1 CH1
    let mut gpioe = periph.GPIOE.split(&mut rcc.ahb);
    let ld3_pwm = gpioe.pe9.into_af2_push_pull(
        &mut gpioe.moder, 
        &mut gpioe.otyper, 
        &mut gpioe.afrh);
    let t1_channels = tim1 (
        periph.TIM1,
        7_200,
        10.Hz(),
        &clocks
    );
    let mut t1_ch1 = t1_channels.0.output_to_pe9(ld3_pwm);
    t1_ch1.set_duty(t1_ch1.get_max_duty() / 2);
    t1_ch1.enable();

    // set up SPI1 for IOX
    let mut gpioa = periph.GPIOA.split(&mut rcc.ahb);
    let pa5_sck = gpioa.pa5.into_af5_push_pull(
        &mut gpioa.moder, 
        &mut gpioa.otyper, 
        &mut gpioa.afrl);
    let pa6_miso = gpioa.pa6.into_af5_push_pull(
        &mut gpioa.moder, 
        &mut gpioa.otyper, 
        &mut gpioa.afrl);
    let pa7_mosi = gpioa.pa7.into_af5_push_pull(
        &mut gpioa.moder, 
        &mut gpioa.otyper, 
        &mut gpioa.afrl);
    let mut pa4_cs = gpioa.pa4.into_push_pull_output(
        &mut gpioa.moder,
        &mut gpioa.otyper);
    let pins = (pa5_sck, pa6_miso, pa7_mosi);
    // hal usually requires generic parameter if instance usage
    // is not provided beforehand just to infer word size
    let mut spi_inst = Spi::new(periph.SPI1, pins, 8.MHz(), clocks, &mut rcc.apb2);

    // set up I2C for display
    // set up I2S for codec

    loop {
        let tx: [u8; 4] = [0x55, 0xAA, 0x01, 0x02];
        let mut rx = tx;
        pa4_cs.set_low().unwrap();
        let _rx_msg = spi_inst.transfer(&mut rx).unwrap();
        pa4_cs.set_low().unwrap();
        asm::delay(8_000_000);

    }
}
