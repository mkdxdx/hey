pub mod simple_synth {
    const MAX_VOICES: usize = 3;

    #[derive(Default)]
    struct Voice {
        time: u32,
        note: u8,
        active: bool,
    }

    #[derive(Default)]
    pub struct Envelope {
        attack: u8,
        delay: u8,
        sustain: u8,
        release: u8,
    }

    pub enum Wave {
        Square,
        Saw,
        Sin,
        Tri
    }

    impl Default for Wave {
        fn default() -> Wave {
            Wave::Square
        }
    }

    #[derive(Default)]
    pub struct Synth {
        voices: [Voice; MAX_VOICES],
        wave: Wave,

        amplifier: Envelope,
        filter_envelope: Envelope,
        filter_frequency: u32,
        filter_resonance: u32,
    }

    fn start_voice(voice: &mut Voice, note: u8) {
        voice.time = 0;
        voice.note = note;
        voice.active = true;
    }

    fn stop_voice(voice: &mut Voice) {
        voice.active = false;
    }

    pub fn synth_init() -> Synth {
        let synth: Synth = Default::default();
        synth
    }

    pub fn synth_process(synth: &mut Synth, dt: u32) {
        for voice in synth.voices.iter_mut() {
            voice.time += dt;
        }
    }

    pub fn synth_start_note(synth: &mut Synth, note: u8) {
        let mut least_active_idx: usize = 0;
        for (pos, voice) in synth.voices.iter().enumerate() {
            if voice.note < synth.voices[least_active_idx].note {
                least_active_idx = pos;
            }
        }
    
        start_voice(&mut synth.voices[least_active_idx], note);
    }
    
    pub fn synth_stop_note(synth: &mut Synth, note: u8) {
        for (pos, voice) in synth.voices.iter_mut().enumerate() {
            if voice.note == note {
                stop_voice(voice);
            }
        }
    }
}
